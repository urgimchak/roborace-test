package com.example.demo;

import static com.example.demo.StringUtils.printStringGroups;

public class DemoApplication {
    /**
     * The entry point to app.
     */
    public static void main(String[] args) {
        String input = "Раз, 2, три,\n" +
                "елочка, гори!\n" +
                "три, 2, раз...\n" +
                "&*!&$*&!heLLO,,,,!*$!,World,,!!!!333\n" +
                "лысый дикобраз\n" +
                "hello,>>333<<<WORLD\n" +
                "world,333!@$@*$,hello,!$!\n" +
                "world,444!@$@*$,hello,!$!\n" +
                "***";

        // process example string
        printStringGroups(input, false);

        // create huge string for test
        // NOTE: JVM error='Cannot allocate memory' may occur
        StringBuilder stringBuilder = new StringBuilder();
        {
            int i = 0;
            while (i < 1000000) {
                stringBuilder.append(input);
                i++;
            }
        }

        printStringGroups(stringBuilder.toString(), true);
    }
}
