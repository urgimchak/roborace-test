package com.example.demo;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * The class contains methods to process strings.
 */
class StringUtils {
    /**
     * Prints to console groups of strings containing same words.
     *
     * @param input         The input string.
     * @param printSizeOnly Whether to print only size of lines group (for huge size group).
     */
    static void printStringGroups(String input, boolean printSizeOnly) {
        AtomicInteger counter = new AtomicInteger(1);
        getGroupedStrings(input).forEach((keys, lines) -> {
            System.out.println("(" + counter.getAndIncrement() + ")");
            if (printSizeOnly) {
                System.out.println(lines.size());
            } else {
                lines.forEach(System.out::println);
            }
        });
    }

    /**
     * Creates map containing set of line words as a key and list of lines with same words as a value.
     *
     * @param input The input string.
     * @return Map containing set of line words as a key and list of lines with same words as a value.
     */
    private static Map<List<String>, List<String>> getGroupedStrings(String input) {
        Map<List<String>, List<String>> result = Arrays.stream(input.split("\n"))
                .parallel()
                .unordered()
                .collect(
                        Collectors.groupingBy(
                                StringUtils::getWordsFromStringSortedByName,
                                Collectors.mapping(s -> s, Collectors.toList())
                        )
                );
        // remove entry with empty list key (line with no words generates empty list key)
        result.remove(Collections.EMPTY_LIST);

        return result;
    }

    /**
     * Gets list of unique words from string ignoring case.
     *
     * @param string The string to get words.
     * @return List of unique words from string ignoring case.
     */
    private static List<String> getWordsFromStringSortedByName(String string) {
        return Arrays.stream(string.split("([^\\p{L}\\p{N}])"))
                .map(String::toLowerCase)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }
}